Orgchart -

1. Install all dependency modules 
2. Enable features module 
3. Enable "Organization Chart" Feature from features interface at "/admin/structure/features"
4. For adding content there are three option 
  a) Enter content manually 
    1. Content -> Add content -> Profile
    2. Profile Photo - Photo which appears in the orgchart.
    3. First Name - First Name of profile
    4. Last Name - Last Name of the profile 
    5. Nick - Nickname of the profile 
    6. Department - Department to which that profile belongs (This is taxonomy vocabulary)
    7. Title - Title/Designation of the profile.
    8. Email - Email address of the profile
    9. Tel - Telephone number of the profile
    10. UPI - Unique Profile ID a unique identity of profile
    11. Super UPI - UPI of superior profile. Keep empty for root profiles.
  b) Import the content using CSV file 
    1. Content -> Add content -> Profile importer CSV
    2. Provide title for importer
    3. Make sure you have your CSV file in the format same as the template from "Download template" file. 
    4. Make sure you have first two columns (both UPI) with same values
    5. Make sure you have leaders first and **************
    6. Select the CSV file for upload.
    7. Click on Save.
    8. Click on Import Tab of node. 
  c) Import the content using JSON file (or feed)
    1. Content -> Add content -> Profile importer JSON
    2. Provide title for importer
    3. Give JSON feed URL
    4. Provide proper "JSONPath Parser Settings" for your JSON feed. Read https://drupal.org/project/feeds_jsonpath_parser for context.
    5. Click on Save.
    6. Click on Import Tab of node. 
5. Ordering profiles - There are two ways profiles can be ordered
  a) Using graphical interface at "/admin/structure/taxonomy/orgchart_hierarchy"
      This is taxonomy page. Using this you can change reporting person as well as order in which the profile will be shown in chart.
  b) Changing Super UPI value. This way you can only change the reporting person i.e. the place where the profile is shown. 
      For changing the order you need to use above 5a method.
5. At "/organization-chart" URL you will get all root level profiles
6. At "/organization-chart/[NID]" URL you will get a root profile (profile which is passed via URL as NID) and all profiles who report to the root profile. 
  For the child profiles if it have further reporting profiles then it will automatically be linked and a class "has-children" will be added to anchor tag.
  