<?php
/**
 * @file
 * orgchart.views.inc
 */

/**
 * Implements hook_views_query_alter().
 */
function orgchart_views_query_alter(&$view, &$query) {
  //Show tree with parent & child
  if ($view->name == 'org_chart_google' && isset($view->args[0]) && is_numeric($view->args[0]) ) {
    $query->add_where(0, 'taxonomy_term_data_taxonomy_term_hierarchy.tid', $view->args[0], '=');
    $query->add_where(0, 'taxonomy_term_data.tid', $view->args[0], '=');
    $query->set_where_group('OR', 0);
  }
  //First rendering which shows all nodes with empty parent (root nodes)
  if ($view->name == 'org_chart_google' && !isset($view->args[0])) {
    $query->add_where(0, db_or()->condition('taxonomy_term_data_taxonomy_term_hierarchy.tid', '', 'IS NULL'));
  }
}