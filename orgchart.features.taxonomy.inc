<?php
/**
 * @file
 * orgchart.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function orgchart_taxonomy_default_vocabularies() {
  return array(
    'orgchart_departments' => array(
      'name' => 'Orgchart Departments',
      'machine_name' => 'orgchart_departments',
      'description' => '',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
    'orgchart_hierarchy' => array(
      'name' => 'Orgchart Hierarchy',
      'machine_name' => 'orgchart_hierarchy',
      'description' => '',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
  );
}
