<?php
/**
 * @file
 * orgchart.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function orgchart_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'org_chart_google';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'taxonomy_term_data';
  $view->human_name = 'org chart google';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'google_chart_tools_views';
  $handler->display->display_options['style_options']['type'] = 'OrgChart';
  $handler->display->display_options['style_options']['width'] = '600';
  $handler->display->display_options['style_options']['height'] = '400';
  $handler->display->display_options['style_options']['isstacked'] = 0;
  $handler->display->display_options['style_options']['pointsize'] = '0';
  /* No results behavior: Global: Text area */
  $handler->display->display_options['empty']['area']['id'] = 'area';
  $handler->display->display_options['empty']['area']['table'] = 'views';
  $handler->display->display_options['empty']['area']['field'] = 'area';
  $handler->display->display_options['empty']['area']['empty'] = TRUE;
  $handler->display->display_options['empty']['area']['content'] = 'No profile to show..!! Please add Profile to be shown here..!!';
  $handler->display->display_options['empty']['area']['format'] = 'plain_text';
  /* Relationship: Taxonomy term: Representative node */
  $handler->display->display_options['relationships']['tid_representative']['id'] = 'tid_representative';
  $handler->display->display_options['relationships']['tid_representative']['table'] = 'taxonomy_term_data';
  $handler->display->display_options['relationships']['tid_representative']['field'] = 'tid_representative';
  $handler->display->display_options['relationships']['tid_representative']['subquery_sort'] = 'node.nid';
  $handler->display->display_options['relationships']['tid_representative']['subquery_view'] = '';
  $handler->display->display_options['relationships']['tid_representative']['subquery_namespace'] = '';
  /* Relationship: Taxonomy term: Parent term */
  $handler->display->display_options['relationships']['parent']['id'] = 'parent';
  $handler->display->display_options['relationships']['parent']['table'] = 'taxonomy_term_hierarchy';
  $handler->display->display_options['relationships']['parent']['field'] = 'parent';
  $handler->display->display_options['relationships']['parent']['label'] = 'Parent term';
  /* Field: Content: Profile Photo */
  $handler->display->display_options['fields']['field_orgchart_profile_photo']['id'] = 'field_orgchart_profile_photo';
  $handler->display->display_options['fields']['field_orgchart_profile_photo']['table'] = 'field_data_field_orgchart_profile_photo';
  $handler->display->display_options['fields']['field_orgchart_profile_photo']['field'] = 'field_orgchart_profile_photo';
  $handler->display->display_options['fields']['field_orgchart_profile_photo']['relationship'] = 'tid_representative';
  $handler->display->display_options['fields']['field_orgchart_profile_photo']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_orgchart_profile_photo']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_orgchart_profile_photo']['settings'] = array(
    'image_style' => 'small_thumbnail',
    'image_link' => '',
  );
  /* Field: Content: Last Name */
  $handler->display->display_options['fields']['field_orgchart_last_name']['id'] = 'field_orgchart_last_name';
  $handler->display->display_options['fields']['field_orgchart_last_name']['table'] = 'field_data_field_orgchart_last_name';
  $handler->display->display_options['fields']['field_orgchart_last_name']['field'] = 'field_orgchart_last_name';
  $handler->display->display_options['fields']['field_orgchart_last_name']['relationship'] = 'tid_representative';
  $handler->display->display_options['fields']['field_orgchart_last_name']['exclude'] = TRUE;
  /* Field: Content: Title */
  $handler->display->display_options['fields']['field_orgchart_title']['id'] = 'field_orgchart_title';
  $handler->display->display_options['fields']['field_orgchart_title']['table'] = 'field_data_field_orgchart_title';
  $handler->display->display_options['fields']['field_orgchart_title']['field'] = 'field_orgchart_title';
  $handler->display->display_options['fields']['field_orgchart_title']['relationship'] = 'tid_representative';
  $handler->display->display_options['fields']['field_orgchart_title']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_orgchart_title']['type'] = 'text_plain';
  /* Field: Content: Department */
  $handler->display->display_options['fields']['field_orgchart_department']['id'] = 'field_orgchart_department';
  $handler->display->display_options['fields']['field_orgchart_department']['table'] = 'field_data_field_orgchart_department';
  $handler->display->display_options['fields']['field_orgchart_department']['field'] = 'field_orgchart_department';
  $handler->display->display_options['fields']['field_orgchart_department']['relationship'] = 'tid_representative';
  $handler->display->display_options['fields']['field_orgchart_department']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_orgchart_department']['type'] = 'taxonomy_term_reference_plain';
  /* Field: Content: Email */
  $handler->display->display_options['fields']['field_orgchart_email']['id'] = 'field_orgchart_email';
  $handler->display->display_options['fields']['field_orgchart_email']['table'] = 'field_data_field_orgchart_email';
  $handler->display->display_options['fields']['field_orgchart_email']['field'] = 'field_orgchart_email';
  $handler->display->display_options['fields']['field_orgchart_email']['relationship'] = 'tid_representative';
  $handler->display->display_options['fields']['field_orgchart_email']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_orgchart_email']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_orgchart_email']['alter']['text'] = '<a href="mailto:[field_email] ">[field_orgchart_email] </a>';
  $handler->display->display_options['fields']['field_orgchart_email']['type'] = 'text_plain';
  /* Field: Content: Tel */
  $handler->display->display_options['fields']['field_orgchart_tel']['id'] = 'field_orgchart_tel';
  $handler->display->display_options['fields']['field_orgchart_tel']['table'] = 'field_data_field_orgchart_tel';
  $handler->display->display_options['fields']['field_orgchart_tel']['field'] = 'field_orgchart_tel';
  $handler->display->display_options['fields']['field_orgchart_tel']['relationship'] = 'tid_representative';
  $handler->display->display_options['fields']['field_orgchart_tel']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_orgchart_tel']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_orgchart_tel']['alter']['text'] = 'Tel : [field_orgchart_tel] ';
  $handler->display->display_options['fields']['field_orgchart_tel']['type'] = 'text_plain';
  /* Field: Taxonomy term: Term ID */
  $handler->display->display_options['fields']['tid']['id'] = 'tid';
  $handler->display->display_options['fields']['tid']['table'] = 'taxonomy_term_data';
  $handler->display->display_options['fields']['tid']['field'] = 'tid';
  /* Field: Content: Author uid */
  $handler->display->display_options['fields']['uid']['id'] = 'uid';
  $handler->display->display_options['fields']['uid']['table'] = 'node';
  $handler->display->display_options['fields']['uid']['field'] = 'uid';
  $handler->display->display_options['fields']['uid']['relationship'] = 'tid_representative';
  $handler->display->display_options['fields']['uid']['exclude'] = TRUE;
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['relationship'] = 'tid_representative';
  $handler->display->display_options['fields']['title']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['title']['alter']['text'] = '<table border="0" class="oc-google-table">
<tr>
	<td rowspan="3"> [field_orgchart_profile_photo] </td>
	<td> <strong>[title] [field_orgchart_last_name]</strong> </td>
</tr>
<tr>
	<td>[field_orgchart_title]</td>
</tr>
<tr>
	<td>[field_orgchart_department]</td>
</tr>
<tr>
	<td colspan="2">[field_orgchart_tel] [field_orgchart_email]</td>
</tr>
</table>';
  $handler->display->display_options['fields']['title']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['title']['alter']['path'] = '[uid]';
  $handler->display->display_options['fields']['title']['alter']['link_class'] = 'has-children';
  $handler->display->display_options['fields']['title']['link_to_node'] = FALSE;
  /* Field: Taxonomy term: Term ID */
  $handler->display->display_options['fields']['tid_1']['id'] = 'tid_1';
  $handler->display->display_options['fields']['tid_1']['table'] = 'taxonomy_term_data';
  $handler->display->display_options['fields']['tid_1']['field'] = 'tid';
  $handler->display->display_options['fields']['tid_1']['relationship'] = 'parent';
  $handler->display->display_options['fields']['tid_1']['hide_empty'] = TRUE;
  /* Filter criterion: Taxonomy vocabulary: Machine name */
  $handler->display->display_options['filters']['machine_name']['id'] = 'machine_name';
  $handler->display->display_options['filters']['machine_name']['table'] = 'taxonomy_vocabulary';
  $handler->display->display_options['filters']['machine_name']['field'] = 'machine_name';
  $handler->display->display_options['filters']['machine_name']['value'] = array(
    'orgchart_hierarchy' => 'orgchart_hierarchy',
  );

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['path'] = 'organization-chart';
  $handler->display->display_options['menu']['type'] = 'normal';
  $handler->display->display_options['menu']['title'] = 'Organization Chart';
  $handler->display->display_options['menu']['weight'] = '5';
  $handler->display->display_options['menu']['name'] = 'main-menu';
  $handler->display->display_options['menu']['context'] = 0;
  $handler->display->display_options['menu']['context_only_inline'] = 0;
  $export['org_chart_google'] = $view;

  return $export;
}
