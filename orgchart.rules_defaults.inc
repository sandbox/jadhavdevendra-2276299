<?php
/**
 * @file
 * orgchart.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function orgchart_default_rules_configuration() {
  $items = array();
  $items['rules_create_assign_term'] = entity_import('rules_config', '{ "rules_create_assign_term" : {
      "LABEL" : "create \\u0026 assign term",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules" ],
      "ON" : { "node_insert--profile" : { "bundle" : "profile" } },
      "DO" : [
        { "entity_query" : {
            "USING" : {
              "type" : "taxonomy_term",
              "property" : "field_orgchart_upi",
              "value" : [ "node:field-orgchart-super-upi" ],
              "limit" : "1"
            },
            "PROVIDE" : { "entity_fetched" : { "fetched_parent_term" : "fetched parent term" } }
          }
        },
        { "entity_create" : {
            "USING" : {
              "type" : "taxonomy_term",
              "param_name" : "[node:title] [node:field-orgchart-last-name]:[node:nid]",
              "param_vocabulary" : "orgchart_hierarchy"
            },
            "PROVIDE" : { "entity_created" : { "created_term" : "Created term" } }
          }
        },
        { "data_set" : {
            "data" : [ "created-term:parent" ],
            "value" : [ "fetched-parent-term" ]
          }
        },
        { "entity_save" : { "data" : [ "created-term" ], "immediate" : 1 } },
        { "data_set" : {
            "data" : [ "node:field-orgchart-taxonomy" ],
            "value" : [ "created-term" ]
          }
        },
        { "entity_save" : { "data" : [ "node" ], "immediate" : 1 } }
      ]
    }
  }');
  $items['rules_delete_profile_and_term'] = entity_import('rules_config', '{ "rules_delete_profile_and_term" : {
      "LABEL" : "Delete profile and term",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules" ],
      "ON" : { "node_delete--profile" : { "bundle" : "profile" } },
      "DO" : [ { "entity_delete" : { "data" : [ "node:field-orgchart-taxonomy" ] } } ]
    }
  }');
  $items['rules_update_profile_term'] = entity_import('rules_config', '{ "rules_update_profile_term" : {
      "LABEL" : "update profile term",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules" ],
      "ON" : { "node_update--profile" : { "bundle" : "profile" } },
      "DO" : [
        { "data_set" : {
            "data" : [ "node:field-orgchart-taxonomy:name" ],
            "value" : "[node:title] [node:field-orgchart-last-name]:[node:nid]"
          }
        },
        { "data_set" : {
            "data" : [ "node:field-orgchart-taxonomy:field-orgchart-upi" ],
            "value" : [ "node:field-orgchart-upi" ]
          }
        },
        { "entity_query" : {
            "USING" : {
              "type" : "taxonomy_term",
              "property" : "field_orgchart_upi",
              "value" : [ "node:field-orgchart-super-upi" ],
              "limit" : "1"
            },
            "PROVIDE" : { "entity_fetched" : { "fetched_parent_term" : "fetched parent term" } }
          }
        },
        { "data_set" : {
            "data" : [ "node:field-orgchart-taxonomy:parent" ],
            "value" : [ "fetched-parent-term" ]
          }
        }
      ]
    }
  }');
  return $items;
}
