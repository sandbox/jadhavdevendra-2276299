<?php
/**
 * @file
 * orgchart.feeds_importer_default.inc
 */

/**
 * Implements hook_feeds_importer_default().
 */
function orgchart_feeds_importer_default() {
  $export = array();

  $feeds_importer = new stdClass();
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'profile_importer';
  $feeds_importer->config = array(
    'name' => 'Profile Importer JSON',
    'description' => 'Import profiles from JSON feeds',
    'fetcher' => array(
      'plugin_key' => 'FeedsHTTPFetcher',
      'config' => array(
        'auto_detect_feeds' => FALSE,
        'use_pubsubhubbub' => FALSE,
        'designated_hub' => '',
        'request_timeout' => NULL,
      ),
    ),
    'parser' => array(
      'plugin_key' => 'FeedsJSONPathParser',
      'config' => array(
        'context' => '',
        'sources' => array(),
        'debug' => array(),
      ),
    ),
    'processor' => array(
      'plugin_key' => 'FeedsNodeProcessor',
      'config' => array(
        'expire' => '-1',
        'author' => 0,
        'authorize' => 0,
        'mappings' => array(
          0 => array(
            'source' => 'jsonpath_parser:0',
            'target' => 'field_orgchart_upi',
            'unique' => FALSE,
          ),
          1 => array(
            'source' => 'jsonpath_parser:1',
            'target' => 'title',
            'unique' => FALSE,
          ),
          2 => array(
            'source' => 'jsonpath_parser:2',
            'target' => 'field_orgchart_nick',
            'unique' => FALSE,
          ),
          3 => array(
            'source' => 'jsonpath_parser:3',
            'target' => 'field_orgchart_title',
            'unique' => FALSE,
          ),
          4 => array(
            'source' => 'jsonpath_parser:4',
            'target' => 'field_orgchart_email',
            'unique' => FALSE,
          ),
          5 => array(
            'source' => 'jsonpath_parser:5',
            'target' => 'field_orgchart_tel',
            'unique' => FALSE,
          ),
          6 => array(
            'source' => 'jsonpath_parser:6',
            'target' => 'guid',
            'unique' => 1,
          ),
          7 => array(
            'source' => 'jsonpath_parser:7',
            'target' => 'field_orgchart_super_upi',
            'unique' => FALSE,
          ),
          8 => array(
            'source' => 'jsonpath_parser:8',
            'target' => 'field_orgchart_last_name',
            'unique' => FALSE,
          ),
          9 => array(
            'source' => 'jsonpath_parser:9',
            'target' => 'field_orgchart_department',
            'term_search' => '0',
            'autocreate' => 1,
          ),
        ),
        'update_existing' => '2',
        'input_format' => 'full_html',
        'skip_hash_check' => 1,
        'bundle' => 'profile',
      ),
    ),
    'content_type' => 'profile_importer_json',
    'update' => 0,
    'import_period' => '-1',
    'expire_period' => 3600,
    'import_on_create' => 0,
    'process_in_background' => 0,
  );
  $export['profile_importer'] = $feeds_importer;

  $feeds_importer = new stdClass();
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'profile_importer_csv';
  $feeds_importer->config = array(
    'name' => 'Profile Importer CSV',
    'description' => 'Import profiles from CSV file',
    'fetcher' => array(
      'plugin_key' => 'FeedsFileFetcher',
      'config' => array(
        'allowed_extensions' => 'txt csv tsv xml opml',
        'direct' => FALSE,
        'directory' => 'public://feeds',
        'allowed_schemes' => array(
          0 => 'public',
        ),
      ),
    ),
    'parser' => array(
      'plugin_key' => 'FeedsCSVParser',
      'config' => array(
        'delimiter' => ',',
        'no_headers' => 0,
      ),
    ),
    'processor' => array(
      'plugin_key' => 'FeedsNodeProcessor',
      'config' => array(
        'expire' => '-1',
        'author' => 0,
        'authorize' => 0,
        'mappings' => array(
          0 => array(
            'source' => 'UPI',
            'target' => 'guid',
            'unique' => 1,
          ),
          1 => array(
            'source' => 'UPI',
            'target' => 'field_orgchart_upi',
            'unique' => FALSE,
          ),
          2 => array(
            'source' => 'Last',
            'target' => 'field_orgchart_last_name',
            'unique' => FALSE,
          ),
          3 => array(
            'source' => 'First',
            'target' => 'title',
            'unique' => FALSE,
          ),
          4 => array(
            'source' => 'Nick',
            'target' => 'field_orgchart_nick',
            'unique' => FALSE,
          ),
          5 => array(
            'source' => 'Dept',
            'target' => 'field_orgchart_department',
            'term_search' => '0',
            'autocreate' => 1,
          ),
          6 => array(
            'source' => 'Title',
            'target' => 'field_orgchart_title',
            'unique' => FALSE,
          ),
          7 => array(
            'source' => 'Email',
            'target' => 'field_orgchart_email',
            'unique' => FALSE,
          ),
          8 => array(
            'source' => 'Tel',
            'target' => 'field_orgchart_tel',
            'unique' => FALSE,
          ),
          9 => array(
            'source' => 'SuperUPI',
            'target' => 'field_orgchart_super_upi',
            'unique' => FALSE,
          ),
        ),
        'update_existing' => '2',
        'input_format' => 'full_html',
        'skip_hash_check' => 1,
        'bundle' => 'profile',
      ),
    ),
    'content_type' => 'profile_importer_csv',
    'update' => 0,
    'import_period' => '-1',
    'expire_period' => 3600,
    'import_on_create' => 0,
    'process_in_background' => 0,
  );
  $export['profile_importer_csv'] = $feeds_importer;

  return $export;
}
